import numpy as np
# statistical concepts will include:
# Mean, Median, Percentiles, Interquartile Range, Outliers, Standard Deviation

scores = [44, 86, 80, 73, 62]
scores_arr = np.array(scores)
mean = np.mean(scores_arr)
print("mean: ", mean)
# Values that don’t fit within the majority of a dataset are known as outliers
# outliers can be identified using sorting => .sort()

# Another key metric that we can use in data analysis is the median.
# The median is the middle value of a dataset that’s been ordered in terms of magnitude (from lowest to highest).
median = np.median(scores_arr)
print("median: ", median)
# The Nth percentile is defined as the point N% of samples lie below it.
# So the point where 40% of samples are below is called the 40th percentile.
# Percentiles are useful measurements because they can tell us where a particular value is situated within the greater dataset.
# The 25th percentile is called the first quartile
# The 50th percentile is called the median
# The 75th percentile is called the third quartile
# The minimum, first quartile, median, third quartile, and maximum of a dataset are called a five-number summary.
# The difference between the first and third quartile is a value called the interquartile range
first_quartile = np.percentile(scores_arr, 25)
print("first quartile: ", first_quartile)
third_quartile = np.percentile(scores_arr, 75)
print("third quartile: ", third_quartile)
fortieth_quartile = np.percentile(scores, 40)
print("fortieth percentile:", fortieth_quartile)


# While the mean and median can tell us about the center of our data.
# They do not reflect the range of the data.
# That's where standard deviation comes in.
standard_deviation = np.std(scores_arr)
print("standard deviation:", standard_deviation)
# The larger the standard deviation, the more spread out our data is from the center.
# The smaller the standard deviation, the more the data is clustered around the mean.