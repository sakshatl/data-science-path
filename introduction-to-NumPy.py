import numpy as np

# NumPy includes a powerful data structure called an array. NumPy array is a special type of list.
# Numpy array looks alot like a python list. unlike python lists, np arrays have only store data of a similar type.
# np random number generator.
np.random.seed(0) # seed set with a value to ensure same random numbers are generated each time code is run
x1 = np.random.randint(10, size=7) # one dimensional array
x2 = np.random.randint(10, size=(3,4)) # two dimensional array
print(x1)
print(x2)


my_array = np.array([10, 20, 30, 40])
my_array2 = np.array([[10, 20, 30 ,40],
                      [50, 60, 70, 80],
                      [90, 100, 110, 120]
                      ])
# Array indexing and accessing single elements
print(my_array[0])
print(my_array2[2,0])

# commonly needed routine is accessing of single rows or columns of an array.
print(my_array2[:, 3])  # accessing the forth column of my_array2
print(my_array2[2, :])  # accessing the third row of my_array2
# result is also stored in the form of an array.


# typically you won't be entering data directly into an array. instead you will be importing it from somewhere else.
# we are able to transform CSV (comma-separated-values) files into array using np.genfromtxt()
# sample.csv => 34.8.7.40, csv_array =  np.genfromtxt('sample.csv', delimiter=',')
data = np.genfromtxt('sample_data.csv', delimiter=',')
print(data)


# np arrays are more efficient than list they allow you to do element wise operations.
list = [1,2,3,4,5]
arr = np.array(list)
arr_double = arr * 2
# np arrays can also be subtracted from or added to each other.
a = np.array([5,6,7,8,9])
b = np.array([10,11,12,13,14])
a_plus_b = a + b